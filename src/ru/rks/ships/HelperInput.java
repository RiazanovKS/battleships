package ru.rks.ships;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Scanner;

/**
 *Класс содержит ввод и проверку введенных координат выстрела.
 *
 * @author Рязанов К.С.
 */

public class HelperInput {
    /**
     * Метод для  ввода координат выстрела.
     * @return введенные координаты выстрела.
     */
    public static String writing() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите координаты выстрела");
        return scanner.nextLine();
    }

    /**
     * Метод возвращает булевое значение в зависимости от результата проверки (строка должна содержать только цифру от
     * 0 до 9) полученных данных.
     * @param string строка, полученная для последующей проверки.
     * @return true- если строка соответствует требованиям проверки, false - если нет.
     */
    public static boolean isCheckup(String string){
        Pattern pattern = Pattern.compile("^[0-9]{1}$");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    /**
     * Метод в котором пользователь должен вводить координаты выстрела до тех пор , пока они не будут соответствовать тре-
     * бованиям проверки ( см. комментарий к методу checkup).
     * @return строка , содержащая цифру от 0 до 9.
     */
    public static String input(){
        String coordinat;
        do{
            coordinat = writing();
        } while (!isCheckup(coordinat));
        return coordinat;
    }
}
