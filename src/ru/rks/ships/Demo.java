package ru.rks.ships;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Класс игры "Морской бой"
 *
 * @author Рязанов К.С.
 */

public class Demo {

    public static void main(String[] args) {
        Ship ship = new Ship();
        ship.setLocation(accommodation());
        int amountOfAttempts = 0;
        String status;
        do {
            System.out.println(status = ship.checkShip(HelperInput.input()));
            amountOfAttempts++;
        } while (status != "Убил");
        System.out.println("Кол-во попыток - " + amountOfAttempts);
    }

    /**
     * Метод возвращает введенные координаты выстрела.
     *
     * @return координаты выстрела строкового типа.
     */

    /**
     * Метод для задания местоположения корабля.
     * @return ArrayList с местоположением корабля
     */
    public static ArrayList<String> accommodation() {
        ArrayList<String> arrayList = new ArrayList<String>();
        int randomnumber = (int) (Math.random() * 4);
        arrayList.add(String.valueOf(randomnumber));
        arrayList.add(String.valueOf(randomnumber + 1));
        arrayList.add(String.valueOf(randomnumber + 2));
        return arrayList;
    }

}
