package ru.rks.ships;

import java.util.Arrays;
import java.util.ArrayList;

/**
 * Класс представления объекта Корабль.
 *
 * @author Рязанов К.С.
 */
public class Ship {
    private ArrayList<String> location;


    public void setLocation(ArrayList<String> location) {
        this.location = location;
    }

    /**
     * Метод возвращает текстовое сообщение пользователю о состоянии корабля в зависимости от полученных координат выстреда.
     *
     * @param shot - координата выстрела
     * @return "Мимо" если координата выстрела не совпадает ни с одной ячейкой корабля;"Ранил" если координата выстрела
     * совпадает с координатой ячейки корабля и у него еще остались клетки:"Убил", если координата выстрела совпала с
     * последней целой ячейкой корабля.
     */
    public String checkShip(String shot) {
        String result = "Мимо";
        int index = location.indexOf(shot);
        if (index != -1) {
            location.remove(index);
            result = "Ранил";
        }
        if (location.isEmpty()) {
            result = "Убил";
        }
        return result;
}

    @Override
    public String toString() {
        return "Ship{" +
                "location=" + location +
                '}';
    }
}

